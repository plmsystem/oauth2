package com.example.demo.auth;

import com.github.scribejava.core.builder.api.DefaultApi20;

public class SnsAPI20 extends DefaultApi20{
	
	private String access_token;
	private String auth;
	
	public SnsAPI20(String access_token, String auth){
		this.access_token = access_token;
		this.auth = auth;
	}
	
	@Override
	public String getAccessTokenEndpoint() {
		return access_token;
	}

	@Override
	protected String getAuthorizationBaseUrl() {
		return auth;
	}

}
