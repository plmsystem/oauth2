package com.example.demo.auth;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.github.scribejava.core.builder.api.DefaultApi20;

import lombok.Data;

@Data
public class SnsValue {
	private String service;
	private String clientId;
	private String clientSecret;
	private String redirectUrl;
	private String profileUrl;
	private String access_token;
	private String auth;
	
	private DefaultApi20 api20Instance;
	
	@Autowired
	private Environment env;
	
	public SnsValue(String service){
		this.service = service;
	}
	
	@PostConstruct
	private void init(){
		this.clientId     = env.getProperty("oauth2." + service + ".client_id");
		this.clientSecret = env.getProperty("oauth2." + service + ".client_secret");
		this.redirectUrl  = env.getProperty("oauth2." + service + ".redirect_url");
		this.profileUrl   = env.getProperty("oauth2." + service + ".profile_url");
		this.access_token = env.getProperty("oauth2." + service + ".access_token");
		this.auth         = env.getProperty("oauth2." + service + ".auth");
		
		if (service.equalsIgnoreCase("naver")){
			this.api20Instance = new SnsAPI20(access_token, auth);
		} else if (service.equalsIgnoreCase("kakao")){
			this.api20Instance = new KakaoAPI20(access_token, auth);
		} else {
			
		}
	}
}
