package com.example.demo.auth;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.model.OAuth2AccessToken;

public class KakaoAPI20 extends SnsAPI20{
	
    public KakaoAPI20(String access_token, String auth) {
		super(access_token, auth);
	}
    
	public OAuth2AccessToken getAccessToken (String client_id, String redirect_uri, String authorize_code) throws Exception {
        BufferedWriter bw = null;
        BufferedReader br = null;
        
        try {
            URL url = new URL(this.getAccessTokenEndpoint());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            
            bw = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
            StringBuilder sb = new StringBuilder();
            sb.append("grant_type=authorization_code");
            sb.append("&client_id=" + client_id);
            sb.append("&redirect_uri=" + redirect_uri);
            sb.append("&code=" + authorize_code);
            bw.write(sb.toString());
            bw.flush();
            
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String line = "";
            String result = "";
            while ((line = br.readLine()) != null) {
                result += line;
            }
            
    		JsonNode rootNode = new ObjectMapper().readTree(result);
            
            String access_Token  = rootNode.get("access_token").asText();
            String refresh_Token = rootNode.get("refresh_token").asText();
            String token_type    = rootNode.get("refresh_token").asText();
            String expires_in    = rootNode.get("expires_in").asText();
            String scope         = rootNode.get("scope").asText();
            
            OAuth2AccessToken token = new OAuth2AccessToken(access_Token, token_type, Integer.parseInt(expires_in), refresh_Token, scope, result);
            return token;
        } finally {
        	 br.close();
             bw.close();
        }
    }
}
