package com.example.demo.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

public class SNSLogin {
	
	Logger logger = LoggerFactory.getLogger(SNSLogin.class);
	
	private OAuth2AccessToken accessToken;
	private OAuth20Service oauthService;
	private SnsValue snsValue;
	private String code;
	
	public SNSLogin(SnsValue snsValue){
		this.snsValue = snsValue;
		
		switch(this.snsValue.getService()){
			case "naver" :
				this.oauthService = new ServiceBuilder(snsValue.getClientId())
								    .apiSecret(snsValue.getClientSecret())
								    .callback(snsValue.getRedirectUrl())
								    .defaultScope("profile")
								    .build(snsValue.getApi20Instance());
				break;
			case "kakao" :
				this.oauthService = new ServiceBuilder(snsValue.getClientId())
								    .callback(snsValue.getRedirectUrl())
								    .responseType("code")
								    .defaultScope("profile,account_email,talk_message")
								    .build(snsValue.getApi20Instance());
				break;
			default :
				//Todo
				break;
		}
	}
	
	public String getAccessToken(){
		return this.accessToken.getAccessToken();
	}
	
	public String getAuthURL() {
		return this.oauthService.getAuthorizationUrl();
	}
	
	private void initAccessToken() throws Exception {
		if (this.snsValue.getService().equalsIgnoreCase("kakao")) {
			this.accessToken = ((KakaoAPI20)this.snsValue.getApi20Instance()).getAccessToken(this.snsValue.getClientId(), this.snsValue.getRedirectUrl(), this.code);
		} else {
			this.accessToken = this.oauthService.getAccessToken(this.code);
		}
		
		logger.info( this.snsValue.getService() +  " Access Token : " + this.accessToken.getAccessToken());
	}

	public User getUserProfile(String code) throws Exception {
		this.code = code;
		this.initAccessToken();
		
		OAuthRequest request = new OAuthRequest(Verb.GET, this.snsValue.getProfileUrl());
		this.oauthService.signRequest(accessToken, request);
		
		Response response = this.oauthService.execute(request);
		return parsonJson(response.getBody());
	}
	
	private User parsonJson(String body) throws Exception{
		System.out.println(body);
		
		String id = null;
		String name = null;
		String email = null;
		
		JsonNode rootNode = new ObjectMapper().readTree(body);
		
		switch(this.snsValue.getService()){
			case "naver" : {
				JsonNode resNode = rootNode.get("response");
				id    = resNode.get("id").asText();
				name  = resNode.get("name").asText();
				email = resNode.get("email") != null ? resNode.get("email").asText() : "";
				break;
			}
			case "kakao" : {
				JsonNode propertiesNode = rootNode.get("properties");
				JsonNode kakao_account = rootNode.get("kakao_account");
				id    = rootNode.get("id").asText();
				name  = propertiesNode.get("nickname").asText();
				email = kakao_account.get("email") != null ? kakao_account.get("email").asText() : "";
				break;
			}
			default : {
				break;
			}
		}
		
		User user = new User();
		user.setId(id);
		user.setEmail(email);
		user.setNickname(name);
		
		return user;
	}
}
