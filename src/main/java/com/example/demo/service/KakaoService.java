package com.example.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.helper.KakaoRestApiHelper;

@Service
public class KakaoService {
	@Autowired
	private KakaoRestApiHelper helper;
	
	public String sendMessage(String accessToken, String message){
        String template_object  = "{"
						        + " \"object_type\": \"text\""
						        + ",\"text\": \"" + message + "\""
						        + ",\"link\": {"
						        + "     \"web_url\": \"https://developers.kakao.com\""
						        + "    ,\"mobile_web_url\": \"https://developers.kakao.com\""
						        + "}"
						        + ",\"button_title\": \"\""
						        + "}";
    
		Map<String, String> paramMap = new HashMap<String, String> ();
		paramMap.put("template_object", template_object);
        
        this.helper.setAccessToken(accessToken);
		return this.helper.sendMessage(paramMap);
	}
}
