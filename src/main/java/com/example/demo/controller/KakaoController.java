package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.auth.SNSLogin;
import com.example.demo.service.KakaoService;

@Controller
public class KakaoController {
	
	Logger logger = LoggerFactory.getLogger(KakaoController.class);
	
	@Autowired
	KakaoService kakaoService;

	@RequestMapping(value="/{snsService}/message", method=RequestMethod.GET)
	public String message(Model model, @PathVariable String snsService, HttpSession session) throws Exception {
		model.addAttribute("snsService", snsService);
		return "sendMessage";
	}
	
	@RequestMapping(value="/{snsService}/sendMessage", method=RequestMethod.POST)
	public String sendMessage(Model model, @PathVariable String snsService, @RequestParam String message, HttpSession session) throws Exception {
		SNSLogin snsLogin = (SNSLogin)session.getAttribute(snsService);
		
		String result = null;
		if (snsService.equalsIgnoreCase("kakao")){
			result = kakaoService.sendMessage(snsLogin.getAccessToken(), message);
		} else if (snsService.equalsIgnoreCase("kakao")){
			
		} else {
			
		}
		
		model.addAttribute("result", result);
		return "result";
	}
}
