package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.auth.SNSLogin;
import com.example.demo.auth.SnsValue;
import com.example.demo.auth.User;

@Controller
public class UserController {
	
	Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	@Qualifier("naverSnsValue")
	private SnsValue naverSnsValue;
	
	@Autowired
	@Qualifier("kakaoSnsValue")
	private SnsValue kakaoSnsValue;
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(Model model, HttpSession session) throws Exception {
		SNSLogin snsNaverLogin = new SNSLogin(naverSnsValue);
		model.addAttribute("naver_url", snsNaverLogin.getAuthURL());
		
		SNSLogin snsKakaoLogin = new SNSLogin(kakaoSnsValue);
		model.addAttribute("kakao_url", snsKakaoLogin.getAuthURL());
		
		return "login";
	}
	
	@RequestMapping(value="/auth/{snsService}/callback", method={RequestMethod.GET, RequestMethod.POST})
	public String snsCallback(Model model, @PathVariable String snsService, @RequestParam String code, HttpSession session) throws Exception {
		//logger.info("=====> service " + snsService + " " + code);

		SNSLogin snsLogin = null;
		if (snsService.equalsIgnoreCase("naver")){
			snsLogin = new SNSLogin(this.naverSnsValue);
		} else if (snsService.equalsIgnoreCase("kakao")){
			snsLogin = new SNSLogin(this.kakaoSnsValue);
		} else {
			
		}
		
		// 1. code를 이용해서 access token 받기
		// 2. access tocken 이용해서 user profile 조회
		User user = snsLogin.getUserProfile(code);
		
		// 3. db 유저 존재 여부 체크
		/*
		if () {
			
		}else{
		
			// 4. 유저 존재 시 강제 로그인 처리
		}
		*/
		
		session.setAttribute(snsService, snsLogin);
		model.addAttribute("result", user);
		model.addAttribute("snsService", snsService);
		
		return "result";
	}
}
