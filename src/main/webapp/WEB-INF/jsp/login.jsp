<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Login</title>
</head>
<body>
	<div>
		<a href="${naver_url}"><img width="300" src="/resources/images/naver-login.png" alt="Naver Login"></a>
		<a href="${kakao_url}"><img width="300" src="/resources/images/kakao-login.png" alt="Kakao Login"></a>
	</div>
</body>
</html>