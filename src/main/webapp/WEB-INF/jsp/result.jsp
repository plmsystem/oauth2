<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>login Result</title>
</head>
<body>
	<div>
		Result : ${result}
	</div>
	
	<div>
		<p>메세지 발송 테스트</p>
		<form id="myForm" action="/${snsService}/sendMessage" method="POST">
			<textarea id="message" name="message" rows="5"></textarea>
		</form>
		<input type="button" value="Send" onclick="myForm.submit();" />
	</div>
</body>
</html>